﻿using UnityEngine;
using System.Collections;

public class ShowInfo : MonoBehaviour {

	public GameObject close;
	public GameObject Arca;
	public GameObject Eksplorasi;

    public Transform grabPosition;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		
		if (other.gameObject.name == "bone3") {
			//close.SetActive (false);
			Arca.SetActive (true);
            Transform arca = Arca.GetComponent<PanelInfo>().arca.transform;
            arca.parent = grabPosition.parent;
            arca.position = grabPosition.position;
            arca.rotation = grabPosition.rotation;
            arca.gameObject.SetActive(true);
            Eksplorasi.SetActive (true);
		}
	}
   
}
