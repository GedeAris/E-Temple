﻿using UnityEngine;
using System.Collections;

public class ShowTutorial : MonoBehaviour {

	public GameObject StartTutorial;
	public GameObject Interaksi;
	public GameObject Eksplorasi;

    public Transform grabPosition;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		
		if (other.gameObject.name == "bone3") {
            Eksplorasi.SetActive(true);
		}
       else
        {
            Eksplorasi.SetActive(false);
        }
	}
   
}
