﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playsound : MonoBehaviour {

    public GameObject genta;
    AudioSource audio;

    public int directionY;
    public int directionZ;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        directionY = 1;
        directionZ = 1;
    }

    void Update()
    {
        if (directionY == 1)
        {
            if (transform.parent.localRotation.eulerAngles.y > 35)
            {
                audio.Play();
                directionY = -1;
            }
        }
        else
        {
            if (transform.parent.localRotation.eulerAngles.y < -35)
            {
                audio.Play();
                directionY = 1;
            }
        }

        if (directionZ == 1)
        {
            if (transform.parent.localRotation.eulerAngles.z > 120)
            {
                audio.Play();
                directionZ = -1;
            }
        }
        else
        {
            if (transform.parent.localRotation.eulerAngles.z < 60)
            {
                audio.Play();
                directionZ = 1;
            }
        }
    }
}
