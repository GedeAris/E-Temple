﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class blinking : MonoBehaviour
{

    public GameObject flashing_Label;
    public RawImage Text;
    public bool fadeIn = true;
    float fadeTime = 0.1f;
    Color colorToFadeTo;

    public float interval;

    void Start()
    {
        StartCoroutine(FadeCoroutine());
        //InvokeRepeating("FlashLabel", 0, interval);
       // StartCoroutine(Toggler());

        
    }

    void FlashLabel()
    {
        if (flashing_Label.activeSelf) { 
            flashing_Label.SetActive(false);

         } 
        else
            flashing_Label.SetActive(true);
       // colorToFadeTo = new Color(1f, 1f, 1f, 0f);
       // Text.CrossFadeColor(colorToFadeTo, fadeTime, true, true);
    }

    IEnumerator FadeCoroutine()
    {
        while (true)
        {
            while (Text.color.a < 1)
            {
                Text.color += new Color(0, 0, 0, Time.deltaTime);
                yield return null;
            }
            while (Text.color.a > 0)
            {
                Text.color -= new Color(0, 0, 0, Time.deltaTime);
                yield return null;
            }
        }
    }

    IEnumerator Toggler()
    {
        yield return new WaitForSeconds(1);
        fadeIn = !fadeIn;

    }

}