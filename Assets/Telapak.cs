﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Telapak : MonoBehaviour {
    private float DelayTime;
    public float defaultDelayTime;
    // Use this for initialization
    void Start () {
        DelayTime = defaultDelayTime;
    }
	
	// Update is called once per frame
	void Update () {
        
            }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Kolam")
        {
            Application.LoadLevel("Uluwatu");
        }
        if (col.gameObject.tag == "Genta")
        {
            Application.LoadLevel("Genta");
        }
        if (col.gameObject.tag == "Wadah Tirta")
        {
            Application.LoadLevel("Wadah Tirta");
        }
        if (col.gameObject.tag == "Pejati")
        {
            Application.LoadLevel("Pejati");
        }
        if (col.gameObject.tag == "FinalProject")
        {
            Application.LoadLevel("FinalProject");
        }
    }

}
