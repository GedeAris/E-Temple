﻿using UnityEngine;
using System.Collections;

using System;
using System.Text; // Process Byte to ASCII
using System.Net; // UDP
using System.Net.Sockets; // UDP

using System.Globalization;

public class GyroRead : MonoBehaviour {

	private UdpClient receivingUdpClient;
	private IPEndPoint RemoteIpEndPoint;

	private Quaternion initialRotation = Quaternion.identity;
	public GameObject PersonHead;

	private float accX;
	private float accY;
	private float accZ;

	private float gyrX;
	private float gyrY;
	private float gyrZ;

	private float magX;
	private float magY;
	private float magZ;

	private float xPos;
	private float yPos;
	private float zPos;

	private float pitch;
	private float yaw;
	private float roll;

	private TcpClient client;
	private Socket server;

	private float dt = 0.01f;
	private float M_PI = 3.14159265358979323846f;

//	private float ACCELEROMETER_SENSITIVITY = 8192.0f;
	private float GYROSCOPE_SENSITIVITY = 65.536f;

		

	// Use this for initialization
	void Start () {
		receivingUdpClient = new UdpClient(5555);
		RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
		/*Int32 port = 5555;
		TcpClient client = new TcpClient("10.122.1.128", port);
		Socket server = new Socket(AddressFamily.InterNetwork,SocketType.Stream, ProtocolType.Tcp);*/

	}
	
	// Update is called once per frame
	void Update () {

		/*byte[] data = new byte[1024];
		NetworkStream stream = client.GetStream();
		data = new Byte[256];
		String responseData = String.Empty;
		Int32 bytes = stream.Read(data, 0, data.Length);
		responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
		Debug.Log("Received: " + responseData);*/         

		if (receivingUdpClient.Available > 0) {
			Byte[] receiveBytes = receivingUdpClient.Receive (ref RemoteIpEndPoint);
			string returnData = Encoding.ASCII.GetString (receiveBytes);
			String[] substrings = returnData.Split (',');

			/*
			// UNITY VIA UDP
			string px = substrings [0].Trim ().Remove(0,1);
			string py = substrings [1].Trim ();
			string pz = substrings [2].Trim ();
			string pw = substrings [3].Trim ().Remove((substrings [3].Length-2), 1);

			float x = float.Parse(px, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
			float y = float.Parse(py, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
			float z = float.Parse(pz, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
			float w = float.Parse(pw, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);

			//Debug.Log(x + "|"+ y + "|"+ z+ "|"+w);
			Quaternion q = new Quaternion (x,y,z,w);

			var att = q * initialRotation;
			att = new Quaternion(att.x, att.y, -att.z, -att.w);
			PersonHead.transform.rotation = Quaternion.Euler (0, 0, 0) * att;
			Debug.Log (att);
			*/


			// USING ORIENTATION AS EULER
			if (substrings.Length == 17 ){
				if(float.Parse(substrings[13].Trim()) == 81){
					//Debug.Log (substrings[14].Trim());

					String[] gX = substrings [14].Trim ().Split ('.');
					String[] gY = substrings [15].Trim ().Split ('.');
					String[] gZ = substrings [16].Trim ().Split ('.');

					gyrX = int.Parse(gX[0]);
					gyrY = int.Parse(gY[0]
					);
					gyrZ = int.Parse(gZ[0]);

					CameraControlGyroO(gyrX, gyrY, gyrZ);
				}
			}

			/*
			// USING ACC & GYRO FOR ESTIMATING EULER ANGLE
			if (substrings.Length >= 9 ){
				accX = float.Parse(substrings[2].Trim());
				accY = float.Parse(substrings[3].Trim());
				accZ = float.Parse(substrings[4].Trim());

				gyrX = float.Parse(substrings[6].Trim());
				gyrY = float.Parse(substrings[7].Trim());
				gyrZ = float.Parse(substrings[8].Trim());

				CameraControlGyroCalc (accX,accY,accZ,gyrX,gyrY,gyrZ);
			}*/

			//Debug.Log(returnData);
			/*
			 // USING ORIENTATION TO QUATERNION
			 if (substrings.Length >= 8 ){
				
				gyrX = float.Parse(substrings[6].Trim());
				gyrY = float.Parse(substrings[7].Trim());
				gyrZ = float.Parse(substrings[8].Trim());

				//if(float.Parse(substrings[9].Trim()) == 81){
					
					CameraControlGyroQ(gyrX, gyrY, gyrZ);
				//}
			}*/
			/*




			/*
			// USING GYRO AS EULER
			if(substrings.Length == 17){

				gyrX = float.Parse(substrings[14].Trim());
				gyrY = float.Parse(substrings[15].Trim());
				gyrZ = float.Parse(substrings[16].Trim());

				if(float.Parse(substrings[13].Trim()) == 81){
					CameraControlGyroO(gyrX, gyrY, gyrZ);
					Debug.Log (gyrX + " | " + gyrY + " | " + gyrZ);
				}
			}*/
			/*
			//
			if (substrings.Length >= 9) {
				accX = float.Parse (substrings [2].Trim ());
				accY = float.Parse (substrings [3].Trim ());
				accZ = float.Parse (substrings [4].Trim ());

				gyrX = float.Parse (substrings [6].Trim ());
				gyrY = float.Parse (substrings [7].Trim ());
				gyrZ = float.Parse (substrings [8].Trim ());

				KalmanFilter ();
			}*/
			/*
			// USING ROTATION VECTOR
			if(substrings.Length == 17){

				gyrX = float.Parse(substrings[14].Trim());
				gyrY = float.Parse(substrings[15].Trim());
				gyrZ = float.Parse(substrings[16].Trim());

				if(float.Parse(substrings[13].Trim()) == 84){
					CameraControlGyroCalc(gyrX, gyrY, gyrZ);
					Debug.Log (gyrX + " | " + gyrY + " | " + gyrZ);
				}
			}*/
			/*
			// USING COMPLEMENTARY FILTER
			if (substrings.Length >= 8 ){
				accX = float.Parse(substrings[2].Trim());
				accY = float.Parse(substrings[3].Trim());
				accZ = float.Parse(substrings[4].Trim());

				gyrX = float.Parse(substrings[6].Trim());
				gyrY = float.Parse(substrings[7].Trim());
				gyrZ = float.Parse(substrings[8].Trim());

				ComplementaryFilter (gyrX,gyrY,gyrZ,accX,accY,accZ);
			}*/





		}
	
	}
	void CameraControlGyroQ(float gyrX, float gyrY, float gyrZ){

		dt = Time.deltaTime;

		Vector3 o = new Vector3 (gyrX, gyrY, gyrZ);
		Vector3 E = o * dt;

		float w = (float)(Math.Cos(E.x/2) * Math.Cos(E.y/2) * Math.Cos(E.z/2) + Math.Sin(E.x/2) * Math.Sin(E.y/2) * Math.Sin(E.z/2));
		float x = (float)(Math.Sin(E.x/2) * Math.Cos(E.y/2) * Math.Cos(E.z/2) - Math.Cos(E.x/2) * Math.Sin(E.y/2) * Math.Sin(E.z/2));
		float y = (float)(Math.Cos(E.x/2) * Math.Sin(E.y/2) * Math.Cos(E.z/2) + Math.Sin(E.x/2) * Math.Cos(E.y/2) * Math.Sin(E.z/2));
		float z = (float)(Math.Cos(E.x/2) * Math.Cos(E.y/2) * Math.Sin(E.z/2) - Math.Sin(E.x/2) * Math.Sin(E.y/2) * Math.Cos(E.z/2));

		Quaternion q = new Quaternion(w, x, y, z);
		//var att = new Quaternion(x, y, -z, 0);
		// PersonHead.transform.rotation = Quaternion.Euler (x, y, z-90);
	Debug.Log(q);
		var att = q * initialRotation;
		att = new Quaternion(att.x, att.y, -att.z, -att.w);
		PersonHead.transform.rotation = Quaternion.Euler (90, 0, 0) * att;
	}

	void CameraControlGyroO(float pitch, float roll, float yaw){
		//PersonHead.transform.eulerAngles = new Vector3(-yaw, pitch, 0 );
		//Quaternion sensor = Quaternion.Euler(new Vector3(yaw, 0, 0 ));
		Vector3 sensor = new Vector3 (-yaw, roll, pitch);
		PersonHead.transform.eulerAngles = sensor;
		//Quaternion Qsensor = Quaternion.Euler(sensor);
		//Quaternion att =  Qsensor * Quaternion.identity;
		//att = new Quaternion(att.x, att.y, -att.z, -att.w);
		//PersonHead.transform.rotation = att;
	}

	void RotationControl(float gyrX, float gyrY, float gyrZ){

		double c1 = Math.Cos(gyrX/2);
		double s1 = Math.Sin(gyrX/2);
		double c2 = Math.Cos(gyrY/2);
		double s2 = Math.Sin(gyrY/2);
		double c3 = Math.Cos(gyrZ/2);
		double s3 = Math.Sin(gyrZ/2);
		double c1c2 = c1*c2;
		double s1s2 = s1*s2;
		float w =(float)(c1c2*c3 - s1s2*s3);
		float x =(float)(c1c2*s3 + s1s2*c3);
		float y =(float)(s1*c2*c3 + c1*s2*s3);
		float z =(float)(c1*s2*c3 - s1*c2*s3);

		Quaternion q = new Quaternion(w, x, y, z);
		//var att = new Quaternion(x, y, -z, 0);
		// PersonHead.transform.rotation = Quaternion.Euler (x, y, z-90);

		var att = q * initialRotation;
		att = new Quaternion(att.x, att.y, -att.z, -att.w);
		PersonHead.transform.rotation = Quaternion.Euler (90, 0, 0) * att;
	}

	void OrientationControl(float eulerX, float eulerY, float eulerZ){
		//PersonHead.transform.eulerAngles = new Vector3(-yaw, pitch, 0 );
		double sx = Math.Sin( eulerX / 2 );
		double sy = Math.Sin( eulerY / 2 );
		double sz = Math.Sin( eulerZ / 2 );
		double cx = Math.Cos( eulerX / 2 );
		double cy = Math.Cos( eulerY / 2 );
		double cz = Math.Cos( eulerZ / 2 );
		double cycz = cy * cz;
		double sysz = sy * sz;
		double d = cycz * cx - sysz * sx;
		double a = cycz * sx + sysz * cx;
		double b = sy * cz * cx + cy * sz * sx;
		double c = cy * sz * cx - sy * cz * sx;

		Quaternion q = new Quaternion( ( float ) a, ( float ) b, ( float ) c, ( float ) d );

		var att = q * initialRotation;
		att = new Quaternion(att.x, att.y, -att.z, -att.w);
		PersonHead.transform.rotation = Quaternion.Euler (0, 0, 0) * att;
	}

	void CameraControlGyroCalc(float ax, float ay, float az, float gx, float gy, float gz){
		//PersonHead.transform.eulerAngles = new Vector3(-yaw, pitch, 0 );

	float pitch = (float)(180 * Math.Atan (ax/Math.Sqrt(ay*ay + az*az))/M_PI);
	float roll = (float)(180 * Math.Atan (ay/Math.Sqrt(ax*ax + az*az))/M_PI);
	float yaw = (float)(180 * Math.Atan (az/Math.Sqrt(ax*ax + az*az))/M_PI);

	Debug.Log (yaw + " | "+ pitch + " | " + roll);

	Vector3 sensor = new Vector3(yaw, 0, 0);
		PersonHead.transform.localEulerAngles = sensor;
	}

	void CameraControlGyroQ2(float eulerX, float eulerY, float eulerZ){

			double sx = Math.Sin( eulerX / 2 );
			double sy = Math.Sin( eulerY / 2 );
			double sz = Math.Sin( eulerZ / 2 );
			double cx = Math.Cos( eulerX / 2 );
			double cy = Math.Cos( eulerY / 2 );
			double cz = Math.Cos( eulerZ / 2 );
			double cycz = cy * cz;
			double sysz = sy * sz;
			double d = cycz * cx - sysz * sx;
			double a = cycz * sx + sysz * cx;
			double b = sy * cz * cx + cy * sz * sx;
			double c = cy * sz * cx - sy * cz * sx;

			Quaternion q = new Quaternion( ( float ) a, ( float ) b, ( float ) c, ( float ) d );

			var att = q * initialRotation;
			att = new Quaternion(att.x, att.y, -att.z, -att.w);
			PersonHead.transform.rotation = Quaternion.Euler (90, 0, 0) * att;
	}

	void KalmanFilter(){

	}

	void ComplementaryFilter(float gyrX, float gyrY, float gyrZ, float accX, float accY, float accZ){

		float pitchAcc, rollAcc, pitch = 0.0f, roll = 0.0f;               

		// Integrate the gyroscope data -> int(angularSpeed) = angle
		pitch += ((float)gyrX / GYROSCOPE_SENSITIVITY) * dt; // Angle around the X-axis
		roll -= ((float)gyrY / GYROSCOPE_SENSITIVITY) * dt;    // Angle around the Y-axis

		// Compensate for drift with accelerometer data if !bullshit
		// Sensitivity = -2 to 2 G at 16Bit -> 2G = 32768 && 0.5G = 8192
		float forceMagnitudeApprox = Math.Abs(accX) + Math.Abs(accY) + Math.Abs(accZ);
		if (forceMagnitudeApprox > 8192 && forceMagnitudeApprox < 32768)
		{
			// Turning around the X axis results in a vector on the Y-axis
		pitchAcc = (float)(Math.Atan2((float)accY, (float)accZ) * 180 / M_PI);
		pitch = pitch * 0.98f + (float)(pitchAcc) * 0.02f;

			// Turning around the Y axis results in a vector on the X-axis
		rollAcc = (float)(Math.Atan2((float)accX, (float)accZ) * 180 / M_PI);
		roll = roll * 0.98f + (float)(rollAcc) * 0.02f;
		}

		Vector3 sensor = new Vector3(0, pitch, roll);
		PersonHead.transform.eulerAngles = sensor;
		Debug.Log (pitch + " | " + roll);
	} 


}
