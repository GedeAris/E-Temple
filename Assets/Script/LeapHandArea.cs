﻿using UnityEngine;
using System.Collections;

public class LeapHandArea : MonoBehaviour
{
    public float speed;
    public bool IsTrigger;
   // public bool ToUp;
    //public bool ToDown;
    public bool ToLeft;
    public bool ToRight;
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController person;
    public float SensivityY;
    public float SensivityX;
    public float rotationalValue;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "palm")
        {
            IsTrigger = true;
            if (ToLeft)
            {
                person.Mouse.YRotation = -speed;
            }
            else if (ToRight)
            {
                person.Mouse.YRotation = speed;
            }
            /* if (ToLeft)
             {
                 transform.eulerAngles = new Vector3(10, SensivityX, 0);
             }

             if (ToRight)
             {
                 transform.eulerAngles = new Vector3(10, SensivityY, 0);
             } */
        }
    }

    void OnTriggerExit(Collider other)
    {
        IsTrigger = false;
        if (ToLeft)
        {
            person.Mouse.YRotation = 0;
        }
            else if (ToRight)
        {
            person.Mouse.YRotation = 0;
        }
    }
}
