﻿
using UnityEngine;
using System.Collections;

public class CameraRaycast : MonoBehaviour
{
    public float theDistance = 10;
    private bool iswalking;
    public GameObject FPScontroller;
    public float DefaultDelayTime;
    private float DelayTime;
    void Start()
    {
        DelayTime = DefaultDelayTime;

    }

    void Update()
    {
        RaycastHit hit;

        Vector3 forward = transform.TransformDirection(Vector3.forward) * theDistance;
        Debug.DrawRay(transform.position, forward, Color.green);

        if (Physics.Raycast(transform.position, forward, out hit, theDistance))
        {

            if (hit.collider.tag == "Jejak")
            {
                DelayTime -= Time.deltaTime;
                if (!iswalking && DelayTime <= 0)
                {
                    Vector3 FixedDestPos = new Vector3(hit.transform.position.x, FPScontroller.transform.position.y, hit.transform.position.z);
                    StartCoroutine(Pindah(FPScontroller.transform.position, FixedDestPos, 2.0f));
                    iswalking = true;

                    DelayTime = DefaultDelayTime;
                }
            }
        }
        else
        {
            DelayTime = DefaultDelayTime;
        }
    }

    IEnumerator Pindah(Vector3 StartPos, Vector3 DestPos, float time)
    {
        float elapsedTime = 0;

        while (elapsedTime < time)
        {
            elapsedTime += Time.deltaTime;
            FPScontroller.transform.position = Vector3.Lerp(StartPos, DestPos, (elapsedTime / time));
            yield return new WaitForEndOfFrame();
        }
        FPScontroller.transform.position = DestPos;
        iswalking = false;
    }

}