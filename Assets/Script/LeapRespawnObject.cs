﻿using UnityEngine;
using System.Collections;

public class LeapRespawnObject : MonoBehaviour {
	public GameObject ParentObject;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnTriggerStay(Collider Other){
		if (Other.gameObject.tag == "Ground") {
			ParentObject.transform.parent = ParentObject.GetComponent<LeapTranslate>().OriginalLocation.transform;
			ParentObject.transform.position = ParentObject.GetComponent<LeapTranslate>().OriginalLocation.transform.position;
			ParentObject.transform.rotation = ParentObject.GetComponent<LeapTranslate>().OriginalLocation.transform.rotation;
			//GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
			GetComponent<Rigidbody> ().velocity = Vector3.zero;
			GetComponent<Rigidbody> ().angularVelocity = Vector3.zero;
		}
		//Debug.Log (Other.gameObject.name);
	}
}
