﻿using UnityEngine;
using System.Collections;

using System;
using System.IO;

public class MeasureTime : MonoBehaviour {

	public GameObject FPS;
	public GameObject ScriptManager;
	public GameObject InitialPosition;
	public GameObject InitialPositionMovement;
	public float FacingTimeSpend;
	public float ForwardBackwardTimeSpend;
	public float MoveTimeSpend;
	public GameObject TargetPosition;
	public GameObject Selection1;
	public float Selection1TimeSpend;
	public GameObject Selection2;
	public float Selection2TimeSpend;
	public GameObject Selection3;
	public float Selection3TimeSpend;
	public GameObject GrabObject1;
	public float Grab1TimeSpend;
	public GameObject GrabObject2;
	public float Grab2TimeSpend;
	public GameObject GrabObject3;
	public float Grab3TimeSpend;
	public string nowCounting = "Nothing";
	public int RecordID = 1;
	public bool Record = false;

	private float Counter = 0.0f;

	// Use this for initialization
	void Start () {
	//	FPS.transform.position = InitialPosition.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	
		Counter = Counter + Time.deltaTime;

		if (Input.GetKeyDown(KeyCode.Backspace)) {
			// Restart Everything
			FPS.transform.position = InitialPosition.transform.position;
			FacingTimeSpend = 0.0f;
			ForwardBackwardTimeSpend = 0.0f;
			MoveTimeSpend = 0.0f;
			Selection1TimeSpend = 0.0f;
			Selection2TimeSpend = 0.0f;
			Selection3TimeSpend = 0.0f;
			Grab1TimeSpend = 0.0f;
			Grab2TimeSpend = 0.0f;
			Grab3TimeSpend = 0.0f;
			nowCounting = "Nothing";
			TargetPosition.SetActive (false);
			ScriptManager.GetComponent<Leap.Unity.LeapStateMonitor> ().rotationalValue = 270.0f;
		}

		if (Input.GetKeyDown (KeyCode.Alpha0)) {
			Counter = 0.0f;
			nowCounting = "Nothing";
			TargetPosition.SetActive (false);
		}
			
		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			nowCounting = "Facing";
			TargetPosition.SetActive (false);
			ScriptManager.GetComponent<Leap.Unity.LeapStateMonitor> ().rotationalValue = 270.0f;
		}

		if (Input.GetKeyDown(KeyCode.Alpha2)) {
			nowCounting = "ForBack";
			TargetPosition.SetActive (false);
			FPS.transform.position = InitialPosition.transform.position;
		}

		if (Input.GetKeyDown(KeyCode.Alpha3)) {
			nowCounting = "Move";
			FPS.transform.position = InitialPositionMovement.transform.position;
			TargetPosition.SetActive (true);
			ScriptManager.GetComponent<Leap.Unity.LeapStateMonitor> ().rotationalValue = 120.0f;
		}

		if (Input.GetKeyDown(KeyCode.Alpha4)) {
			nowCounting = "Selection1";
			FPS.transform.position = Selection1.transform.position;
			TargetPosition.SetActive (false);
			ScriptManager.GetComponent<Leap.Unity.LeapStateMonitor> ().rotationalValue = 270.0f;
		}

		if (Input.GetKeyDown(KeyCode.Alpha5)) {
			nowCounting = "Selection2";
			FPS.transform.position = Selection2.transform.position;
			TargetPosition.SetActive (false);
			ScriptManager.GetComponent<Leap.Unity.LeapStateMonitor> ().rotationalValue = 0.0f;
		}

		if (Input.GetKeyDown(KeyCode.Alpha6)) {
			nowCounting = "Selection3";
			FPS.transform.position = Selection3.transform.position;
			TargetPosition.SetActive (false);
			ScriptManager.GetComponent<Leap.Unity.LeapStateMonitor> ().rotationalValue = 30.0f;
		}

		if (Input.GetKeyDown(KeyCode.Alpha7)) {
			nowCounting = "Grab1";
			FPS.transform.position = GrabObject1.transform.position;
			TargetPosition.SetActive (false);
			ScriptManager.GetComponent<Leap.Unity.LeapStateMonitor> ().rotationalValue = 180.0f;
		}

		if (Input.GetKeyDown(KeyCode.Alpha8)) {
			nowCounting = "Grab2";
			FPS.transform.position = GrabObject2.transform.position;
			TargetPosition.SetActive (false);
			ScriptManager.GetComponent<Leap.Unity.LeapStateMonitor> ().rotationalValue = 180.0f;
		}

		if (Input.GetKeyDown(KeyCode.Alpha9)) {
			nowCounting = "Grab3";
			FPS.transform.position = GrabObject3.transform.position;
			TargetPosition.SetActive (false);
			ScriptManager.GetComponent<Leap.Unity.LeapStateMonitor> ().rotationalValue = 280.0f;
		}
		if (Record) {
			switch (nowCounting) {
			case "Facing":
				FacingTimeSpend = Counter;
				if (ScriptManager.GetComponent<Leap.Unity.LeapStateMonitor> ().rotationalValue > 450.0f) {
					Debug.Log ("STOP DIR RIGHT");
				}
				if (ScriptManager.GetComponent<Leap.Unity.LeapStateMonitor> ().rotationalValue < 270.0f) {
					Debug.Log ("STOP DIR LEFT");
				}
				break;
			case "ForBack":
				ForwardBackwardTimeSpend = Counter;
				break;
			case "Move":
				MoveTimeSpend = Counter;
				break;
			case "Selection1":
				Selection1TimeSpend = Counter;
				break;
			case "Selection2":
				Selection2TimeSpend = Counter;
				break;
			case "Selection3":
				Selection3TimeSpend = Counter;
				break;
			case "Grab1":
				Grab1TimeSpend = Counter;
				break;
			case "Grab2":
				Grab2TimeSpend = Counter;
				break;
			case "Grab3":
				Grab3TimeSpend = Counter;
				break;
			default:
				break;
			}
		}

		if (Input.GetKeyDown (KeyCode.R)) {
			Counter = 0.0f;
			Record = true;
		}

		if (Input.GetKeyDown (KeyCode.T)) {
			Record = false;
		}
	
		if (Input.GetKeyDown(KeyCode.S)) {
			string path;
			path = @"C:\Unity\Data\Test\" + RecordID + "_TestRecord.txt";
			// This text is added only once to the file.
			if (!File.Exists (path)) {
				// Create a file to write to.
				using (StreamWriter sw = File.CreateText (path)) {
					sw.WriteLine ("Partisipan,Facing,ForwardBackward,Move,Selection1,Selection2,Selection3,Grab1,Grab2,Grab3,");
				}
			} else {
				RecordID++;
			}
			path = @"C:\Unity\Data\Test\" + RecordID + "_TestRecord.txt";
				using (StreamWriter sw = File.AppendText (path)) {
				sw.WriteLine (RecordID + "," + FacingTimeSpend + "," + ForwardBackwardTimeSpend + "," + MoveTimeSpend + "," + Selection1TimeSpend + "," + Selection2TimeSpend + "," + Selection3TimeSpend + ","+ Grab1TimeSpend + ","+ Grab2TimeSpend + ","+ Grab3TimeSpend + ",");
				}	
			}
		}
	
}
