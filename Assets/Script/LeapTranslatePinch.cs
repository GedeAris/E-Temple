﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class LeapTranslatePinch : MonoBehaviour {

	public string ObjectID;
	public bool Highlight;
	public GameObject OriginalLocation;
	public string GrabBy;
	public float MaxAngle;

	private float TimeTotal;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		Logger ();
	}

	void OnTriggerEnter(Collider Other){
		Debug.Log (Other.gameObject.name);
		if (Other.gameObject.name == "palm") {
			if (GetComponent<Rigidbody> ()) {
				Destroy (GetComponent<Rigidbody> ());
			}
			Highlight = true;
			switch (Other.gameObject.transform.GetChild (0).name) {
			case "HandleR":
				GrabBy = "Right";
				break;
			case "HandleL":
				GrabBy = "Left";
				break;
			}
		}
		if (Other.gameObject.tag == "Ground") {
			transform.parent = OriginalLocation.transform;
			transform.position = OriginalLocation.transform.position;
			transform.rotation = OriginalLocation.transform.rotation;
			//GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
			GetComponent<Rigidbody> ().velocity = Vector3.zero;
			GetComponent<Rigidbody> ().angularVelocity = Vector3.zero;
		}
		//Debug.Log (Other.gameObject.name);
	}
	void OnTriggerStay(Collider Other){
		//if (Other.gameObject.name == "palm") {
		//Destroy (GetComponent<Rigidbody>());
		//Grabbed = true;
		//}
	}

	void OnTriggerExit(Collider Other){
		if (Other.gameObject.name == "palm" && !GetComponent<Rigidbody>()) {
			gameObject.AddComponent<Rigidbody>();
		}
		Highlight = false;
		//GrabBy = "None";
	}

	void Logger(){
		bool Recording = GameObject.Find ("ScriptManager").GetComponent<Leap.Unity.LeapStateMonitor>().DataRecord;
		int IDRecording = GameObject.Find ("ScriptManager").GetComponent<Leap.Unity.LeapStateMonitor>().DataRecordID;
		TimeTotal = TimeTotal + Time.deltaTime;

		string path;
		if (!Recording) {
			path = @"C:\Unity\Data\" + IDRecording + "_Object_" + ObjectID + ".txt";
			// This text is added only once to the file.
			if (!File.Exists (path)) {
				// Create a file to write to.
				using (StreamWriter sw = File.CreateText (path)) {
					sw.WriteLine (",,Object,,,,,,,,,,,,");
					sw.WriteLine (",,,,,,Position,,,Rotation,,,Scale,,");
					sw.WriteLine ("Frame,Time,Name,Tag,AsTrigger,Interact,X,Y,Z,X,Y;Z,X,Y,Z,");
				}
			}
		}


		// This text is always added, making the file longer over time
		// if it is not deleted.
		path = @"C:\Unity\Data\" + IDRecording + "_Object_" + ObjectID + ".txt";
		using (StreamWriter sw = File.AppendText (path)) {
			sw.WriteLine (Time.frameCount + "," + TimeTotal + "," + ObjectID + "," + tag + ",-,-," + transform.position.x + "," + transform.position.y + "," + transform.position.z + "," + transform.rotation.x + "," + transform.rotation.y + "," + transform.rotation.z + "," + transform.lossyScale.x + "," + transform.lossyScale.y + "," + transform.lossyScale.z + ",");
		}	
	}

}
