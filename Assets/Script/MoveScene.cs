﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MoveScene : MonoBehaviour {

    public static MoveScene Instance;

    [SerializeField] GameObject Button_Genta;
    [SerializeField] GameObject Button_Pejati;
    [SerializeField] GameObject Button_Wadah_Tirta;
    [SerializeField] GameObject Button_Back;
    //public Transform rayOrigin;
    float timeLeftButtonExit = 2.0f;

    void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        } else
        {
            Instance = this;
        }
    }

	// Use this for initialization
	void Start () {
    
    }

    public void LoadLevelWithFade(string LoadedLevel)
    {
        StartCoroutine(LoadLevelWithFadeCoroutine(LoadedLevel));
    }

    IEnumerator LoadLevelWithFadeCoroutine(string LoadedLevel)
    {
        if (GetComponent<Fading>())
        {
            float fadetime = GetComponent<Fading>().BeginFade(1);
            yield return new WaitForSeconds(fadetime);
            SceneManager.LoadScene(LoadedLevel);
        }
    }
}
