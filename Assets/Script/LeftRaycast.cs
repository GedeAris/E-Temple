﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftRaycast : MonoBehaviour
{
    public float theDistance;
    private bool isactive;
    //public GameObject Panel;
    private float DelayTime;
    public float defaultDelayTime;
    //public Handheld HandleR;

    public Transform rayOrigin;

    // Use this for initialization
    void Start()
    {
        DelayTime = defaultDelayTime;
        // HandleR = GetComponent<Handheld>();   
    }

    // Update is called once per frame
    void Update()
    {
        Quaternion newRot = Camera.main.transform.rotation;
        newRot = Quaternion.Euler(0, newRot.eulerAngles.y, newRot.eulerAngles.z);
        Camera.main.transform.rotation = newRot;
        // Vector3 rayDirection = Camera.main.transform.forward;
        Vector3 rayDirection = rayOrigin.transform.forward;
        Ray ray = new Ray(rayOrigin.position, rayDirection);
        // Ray ray = GetComponent<HandModel>().SetLeapHand(new Vector3(0.5f, 0.5f, 0));

        RaycastHit hit = new RaycastHit();

        Debug.DrawRay(rayOrigin.position, rayDirection * theDistance, Color.red);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            DelayTime -= Time.deltaTime;
          /*  if (hit.collider.gameObject.tag == "Info" && DelayTime <= 0)
            {
                hit.collider.gameObject.GetComponent<InfoButton>().info.SetActive(true);

                DelayTime = defaultDelayTime;
            } */
            if (hit.collider.gameObject.tag == "PanelInfo" && DelayTime <= 0)
               {
                PanelInfo info = hit.collider.gameObject.GetComponent<PanelInfo>();
                info.Panel.SetActive(false);
                Transform arca = info.arca.transform;
                arca.parent = info.Panel.transform;
                arca.gameObject.SetActive(false);
            }
            DelayTime -= Time.deltaTime;
            if (hit.collider.gameObject.tag == "FinalProject")
            {
                if (DelayTime > 0)
                {
                    DelayTime -= Time.deltaTime;
                }
                else
                {
                    //Debug.Log("MASOK");
                    ButtonTriggered buttonTriggered = hit.collider.gameObject.GetComponent<ButtonTriggered>();
                    MoveScene.Instance.LoadLevelWithFade("FinalProject");
                    //buttonTriggered.scene.SetActive(true);
                }
            }
            
        }
        else { DelayTime = defaultDelayTime; }
    }

    /*    void OnTriggerStay(Collider col)
        {
            if (col.gameObject.tag == "Info")
            {

            }
        }*/
}
