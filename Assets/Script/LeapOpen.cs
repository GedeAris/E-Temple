﻿using UnityEngine;
using System.Collections;

public class LeapOpen : MonoBehaviour {

	public GameObject Open;

	void OnTriggerExit(Collider Other){
		if (Other.gameObject.tag == "Hand") {
			Open.SetActive (true);
		}
		Debug.Log(Other);
	}
}
