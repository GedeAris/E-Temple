﻿using UnityEngine;
using System.Collections;
using Leap;

// Leap Gesture Orion

namespace Leap.Unity{
	/*public class LeapConfig : MonoBehaviour {

		public static bool KeyTapState;
		public static bool ScreenTapState;
		private GameObject[] AllHMBox;

		Controller controller;
		// Use this for initialization
		void Start () {
			controller = new Controller();
			controller.EnableGesture(Gesture.GestureType.TYPE_KEY_TAP);
			controller.Config.SetFloat("Gesture.KeyTap.MinDownVelocity", 40.0f);
			controller.Config.SetFloat("Gesture.KeyTap.HistorySeconds", .2f);
			controller.Config.SetFloat("Gesture.KeyTap.MinDistance", 1.0f);
			controller.Config.Save();

			controller.EnableGesture(Gesture.GestureType.TYPE_CIRCLE);
			controller.Config.SetFloat("Gesture.Circle.MinRadius", 210.0f);
			controller.Config.SetFloat("Gesture.Circle.MinArc", 2.0f);
			controller.Config.Save();

			controller.EnableGesture (Gesture.GestureType.TYPE_SWIPE);
			controller.Config.SetFloat ("Gesture.Swipe.MinLength", 150.0f);
			controller.Config.SetFloat ("Gesture.Swipe.MinVelocity", 1000f);
			controller.Config.Save ();

			controller.EnableGesture (Gesture.GestureType.TYPE_SCREEN_TAP);
			controller.Config.SetFloat ("Gesture.ScreenTap.MinForwardVelocity", 40.0f);
			controller.Config.SetFloat ("Gesture.ScreenTap.HistorySeconds", .5f);
			controller.Config.SetFloat ("Gesture.ScreenTap.MinDistance", 1.5f);
			controller.Config.Save ();
		}

		// Update is called once per frame
		void Update () {
			Frame frame = controller.Frame();
			if(frame.Gestures().Count == 0){
				ScreenTapState = false;
				KeyTapState = false;
			}
			Debug.Log(frame.Gestures().Count);
			for (int g = 0; g < frame.Gestures().Count; g++) {
				switch (frame.Gestures()[g].Type) {
				// case Gesture.GestureType.TYPE_CIRCLE:
				// 	Debug.Log("Circle!");
				// 	break;
				case Gesture.GestureType.TYPE_KEY_TAP:
					Debug.Log("KeyTap!");
					KeyTapState = true;
					break;
				case Gesture.GestureType.TYPE_SCREEN_TAP:
					Debug.Log("SceenTap!");
					ScreenTapState = true;
					break;
				case Gesture.GestureType.TYPE_SWIPE:
					Debug.Log("Swipe!");
					SwipeGesture swipeGesture = new SwipeGesture (frame.Gestures()[g]);
					Vector swipeDirection = swipeGesture.Direction;
					// Debug.Log(swipeDirection);
					if(swipeDirection.x < -0.75f){
						// Sembunyikan HM Box jika swipe terpenuhi
						AllHMBox = GameObject.FindGameObjectsWithTag("HMBox");
						foreach (GameObject i in AllHMBox){
							i.SetActive(false);
						}
					}
					break;
				default:
					Debug.Log("Nothing");
					KeyTapState = false;
					ScreenTapState = false;
					break;
				}
			}
		}
	}*/
}
