﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HandRaycast : MonoBehaviour {
    public float theDistance;
    private bool isactive;
    public GameObject Panel;
    private float DelayTime;
    public float defaultDelayTime;
    //public Handheld HandleR;

    public Transform rayOrigin;
    public Transform grabPosition;

	// Use this for initialization
	void Start () {
        DelayTime = defaultDelayTime;
       // HandleR = GetComponent<Handheld>();   
	}
	
	// Update is called once per frame
	void Update () {
        Quaternion newRot = Camera.main.transform.rotation;
        newRot = Quaternion.Euler(0, newRot.eulerAngles.y, newRot.eulerAngles.z);
        Camera.main.transform.rotation = newRot;
        // Vector3 rayDirection = Camera.main.transform.forward;
        Vector3 rayDirection = rayOrigin.transform.forward;
        Ray ray = new Ray(rayOrigin.position, rayDirection);
        // Ray ray = GetComponent<HandModel>().SetLeapHand(new Vector3(0.5f, 0.5f, 0));
        
        RaycastHit hit = new RaycastHit();

        Debug.DrawRay(rayOrigin.position, rayDirection * theDistance, Color.cyan);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            if (hit.collider.gameObject.tag == "Info")
            {
                if (DelayTime > 0)
                {
                    DelayTime -= Time.deltaTime;
                }
                else
                {
                    InfoButton infoButton = hit.collider.gameObject.GetComponent<InfoButton>();
                    infoButton.info.SetActive(true);
                    Debug.Log("Masuk");
                    Transform arca = infoButton.info.GetComponent<PanelInfo>().arca.transform;
                    arca.parent = grabPosition.parent;
                    arca.position = grabPosition.position;
                    arca.rotation = grabPosition.rotation;
                    arca.gameObject.SetActive(true);
                    DelayTime = defaultDelayTime;
                }
            } 

            //pindah scene pejati
            if (hit.collider.gameObject.tag == "Pejati")
            {
                if (DelayTime > 0)
                {
                    DelayTime -= Time.deltaTime;
                }
                else
                {
                    Debug.Log("MASOK");
                    ButtonTriggered buttonTriggered = hit.collider.gameObject.GetComponent<ButtonTriggered>();
                    MoveScene.Instance.LoadLevelWithFade("Pejati");
                    DelayTime = defaultDelayTime;
                }
            }

            //pindah scene Genta
            if (hit.collider.gameObject.tag == "Genta")
            {
                if (DelayTime > 0)
                {
                    DelayTime -= Time.deltaTime;
                }
                else
                {
                    Debug.Log("MASOK");
                    ButtonTriggered buttonTriggered = hit.collider.gameObject.GetComponent<ButtonTriggered>();
                    MoveScene.Instance.LoadLevelWithFade("Genta");
                    DelayTime = defaultDelayTime;
                }
            }

            //pindah scene Wadah Tirta
            if (hit.collider.gameObject.tag == "Wadah Tirta")
            {
                if (DelayTime > 0)
                {
                    DelayTime -= Time.deltaTime;
                }
                else
                {
                    Debug.Log("MASOK");
                    ButtonTriggered buttonTriggered = hit.collider.gameObject.GetComponent<ButtonTriggered>();
                    MoveScene.Instance.LoadLevelWithFade("Wadah Tirta");
                    DelayTime = defaultDelayTime;
                }
            }

            //mainmenu
            if (hit.collider.gameObject.tag == "Play")
            {
                if (DelayTime > 0)
                {
                    DelayTime -= Time.deltaTime;
                }
                else
                {
                    //Debug.Log("MASOK");
                    ButtonTriggered buttonTriggered = hit.collider.gameObject.GetComponent<ButtonTriggered>();
                    MoveScene.Instance.LoadLevelWithFade("FinalProject");
                    // DelayTime = defaultDelayTime;
                }
            }
        }
        else { DelayTime = defaultDelayTime; }

    }
}
