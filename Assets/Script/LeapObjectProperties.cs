﻿using UnityEngine;
using System.Collections;

public class LeapObjectProperties : MonoBehaviour {
	public GameObject Highlight;
	public GameObject BoxDescription;
	public GameObject ObjectMain;
	public GameObject Helper;
	public string ObjectName;
	public string ObjectDescription;
	public bool Focus;
	private GameObject OriginalLocation;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Focus) {
			Highlight.SetActive (true);
			GameObject obj = GameObject.Find ("ScriptManager");
			if (obj.GetComponent<Leap.Unity.LeapStateMonitor> ().TimeGazeR > 3.0f || obj.GetComponent<Leap.Unity.LeapStateMonitor> ().TimeGazeL > 3.0f) {
				BoxDescription.SetActive (true);
				//ObjectMain.SetActive (true);
			}
			if (Input.GetKeyDown(KeyCode.E)) {
				BoxDescription.SetActive (true);
			}
		} else {
			Highlight.SetActive (false);
		}
	}

}
