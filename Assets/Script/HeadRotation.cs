﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;
using System;
using System.IO;

namespace Leap.Unity
{

    public class HeadRotation : MonoBehaviour
    {
        private LeapProvider provider;
        private LeapHandController controller;
        private HandModel PlayerHand;
        public Camera PersonEye;
        public GameObject person;
        public GameObject PersonHead;
        public GameObject HandleLeft;
        public GameObject HandleRight;
        public GameObject HandArea;

        public bool ToLeft;
        public bool ToRight;

        public float zIndex;
        public float SensivityY;
        public float SensivityX;
        public float TimeGazeR;
        public float TimeGazeL;

        public float rotationalValue = 270.0f;

        private Quaternion initialRotation = Quaternion.identity;
        private Vector3 PalmLeft;
        private Vector3 PalmRight;
        private Vector3 PalmRightDirection;

        private float yaw = 0.0f;
        private float deltaTime;

        void Movement(Frame frame)
        {
            Hand leftHand = null;
            Hand rightHand = null;
            // Tentukan tangan
            foreach (Hand hand in frame.Hands)
            {
                if (hand.IsLeft)
                {
                    leftHand = hand;
                }
                if (hand.IsRight)
                {
                    rightHand = hand;
                }
            }
            Vector3 RightHandZ = new Vector3(rightHand.PalmPosition.x, rightHand.PalmPosition.y, rightHand.PalmPosition.z);
            Vector3 LeftHandZ = new Vector3(leftHand.PalmPosition.x, leftHand.PalmPosition.y, leftHand.PalmPosition.z);
            Vector3 personZ = person.transform.position;
        }

        void PersonDirectionControl(Hand hand)
        { // Stand Mode False
            bool TriggeredR = GameObject.Find("HandAreaR").GetComponent<LeapHandArea>().IsTrigger;
            bool TriggeredL = GameObject.Find("HandAreaL").GetComponent<LeapHandArea>().IsTrigger;
           // bool TriggeredT = GameObject.Find("HandAreaT").GetComponent<LeapHandArea>().IsTrigger;
          //  bool TriggeredD = GameObject.Find("HandAreaD").GetComponent<LeapHandArea>().IsTrigger;

        //    bool Up = GameObject.Find("HandAreaT").GetComponent<LeapHandArea>().ToUp;
        //    bool Down = GameObject.Find("HandAreaD").GetComponent<LeapHandArea>().ToDown;
            bool Left = GameObject.Find("HandAreaL").GetComponent<LeapHandArea>().ToLeft;
            bool Right = GameObject.Find("HandAreaR").GetComponent<LeapHandArea>().ToRight;
         //   Debug.Log("Kena");
            if (hand.Confidence > 0.95f)
            {
                if (ToLeft)
                {
                    transform.eulerAngles = new Vector3(10, SensivityX, 0);
                }

                if (ToRight)
                {
                    transform.eulerAngles = new Vector3(10, SensivityY, 0);
                }
                float RotationX = PersonEye.transform.localEulerAngles.x;
                /*   if (Up && TriggeredT && (RotationX > 315 || RotationX < 46))
                    {
                        PersonEye.transform.RotateAround(person.transform.right, -0.01f * SensivityY);
                    }
                    if (Down && TriggeredD && (RotationX < 45 || RotationX > 314))
                    {
                        PersonEye.transform.RotateAround(person.transform.right, 0.01f * SensivityY);
                    }  */

               // Debug.Log("string Kena");
            }
        }
    }
}