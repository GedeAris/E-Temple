﻿using UnityEngine;
using System.Collections;

public class ObjectProperties : MonoBehaviour {
	public bool Focus;
	public GameObject Highlight;
	public GameObject BoxDescription;
	public GameObject Helper;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (Focus) {
			Highlight.SetActive (true);
			if (Input.GetKeyDown(KeyCode.E)) {
				BoxDescription.SetActive (true);
			}
		} else {
			Highlight.SetActive (false);
		}
	}

}
