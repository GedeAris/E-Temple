﻿using UnityEngine;
using System.Collections;
using Leap;
//using UnityEditor;
using System;
using System.IO;

namespace Leap.Unity{
	public class LeapStateMonitor : MonoBehaviour {
		private LeapProvider provider;
		private LeapHandController controller;
		private HandModel PlayerHand;
		public Camera PersonEye;
		public GameObject person;
		public GameObject PersonHead;
		public GameObject HandleLeft;
		public GameObject HandleRight;
		public GameObject Gear;
		public GameObject HandArea;
		//public bool StandMode = false;
		public bool LeapMode = true;
		public bool isGrab = false;
		//public bool isPinch = false;
		public float SensivityZ;
		public bool DynamicMovementSpeed;
		public float MoveSpeed;
		public float zIndex;
		private GameObject[] AllHighlight;
		public float SensivityX;
		public float SensivityY;
		public float TimeGazeR;
		public float TimeGazeL;
		public float rangeRaycast;
		public float GrabToleranceWalk;
		public bool DataRecord = false;
		public int DataRecordID = 0;
		public bool GrabPinch; 

		public bool KeyboardMouseMode = false;
		public float rotationalValue = 270.0f;

		private bool StateScanning = false;
		private float TotalTime;
		/*private bool remoteCommunicating = false;
		private bool RemoteCommunicating {
			get {
				if (!remoteCommunicating) {
					remoteCommunicating = EditorApplication.isRemoteConnected;
				}
				return remoteCommunicating;
			}
		}*/

		private Quaternion initialRotation = Quaternion.identity;
		private Vector3 PalmLeft;
		private Vector3 PalmRight;
		private Vector3 PalmRightDirection;
		private float GrabAngleLeft;
		private float GrabAngleRight;
		private float PinchDistanceLeft;
		private float PinchDistanceRight;

		private float yaw = 0.0f;
		private bool StateMove;
		private Gyroscope gyro;

		private	float deltaTime;

		void Start ()
		{
			provider = FindObjectOfType<LeapProvider>() as LeapProvider;
			if (SystemInfo.supportsGyroscope) {
				Input.gyro.enabled = true;
			}
		}

		void Update ()
		{

			if (KeyboardMouseMode) {
				EnableKeyboardMouse ();
			//	EnableRaycast ();
				EnableCloseInfo ();
			} else {
				Frame frame = provider.CurrentFrame;
				// Movement State
				if (frame.Hands.Count == 2 && !GrabPinch) {
					Movement (frame);
				}
				// Hand Gear Activation
				if (frame.Hands.Count > 0) {
					foreach (Hand hand in frame.Hands) {
						//NormalDirection (hand);
					}
				}

				foreach (Hand hand in frame.Hands) {
					if (!StateMove && !GrabPinch) {
						Scanning (hand);
						ScanningInspect (hand);
						GrabObject (hand);
						//PinchObject (hand);
					//	ThumbToClose (hand);
					}
					
					if (!StateMove && GrabPinch) {
						GrabObject (hand);
						//PinchObject (hand);
					}
				//	PersonDirectionControl (hand);
					NormalDirection (hand);
					//LoggerReliabilityConf(hand);
				}
			}
			CameraControlGyro (); 
			
			//LoggerHand (frame);
			/*if (EditorApplication.isRemoteConnected) {
				LoggerReliability ( "1");
			} else {
				LoggerReliability ( "0");
			}*/
			TotalTime = TotalTime + Time.deltaTime;
		}

		void Movement(Frame frame){
			Hand leftHand = null;
			Hand rightHand = null;
			// Tentukan tangan
			foreach (Hand hand in frame.Hands) {
				if (hand.IsLeft) {
					leftHand = hand;
				}
				if (hand.IsRight) {
					rightHand = hand;
				}
				//Logger ("Movement", hand);
			}
			Vector3 RightHandZ = new Vector3( rightHand.PalmPosition.x ,  rightHand.PalmPosition.y ,  rightHand.PalmPosition.z);
			Vector3 LeftHandZ = new Vector3( leftHand.PalmPosition.x ,  leftHand.PalmPosition.y ,  leftHand.PalmPosition.z);
			Vector3 personZ = person.transform.position;

			// Menghitung jarak dari telapak tangan ke badan
			float distanceR = Vector3.Distance(RightHandZ, personZ);
			float distanceL = Vector3.Distance(LeftHandZ, personZ);
			//Debug.Log(Mathf.Abs(distanceL - distanceR));

			// Bandingkan posisi, jika kiri lebih depan ketimbang kanan, mundur
			if(distanceL > distanceR && Mathf.Abs(distanceL - distanceR) > SensivityZ && rightHand.GrabAngle > GrabToleranceWalk  && leftHand.GrabAngle >  GrabToleranceWalk){
				if (DynamicMovementSpeed) {
					person.transform.position -= PersonHead.transform.forward * 0.1f*( Mathf.Abs(distanceL - distanceR)) * MoveSpeed;
				} else {
					person.transform.position -= PersonHead.transform.forward * 0.01f * MoveSpeed;
				}
				StateMove = true;
				//LoggerState ( "R", "Move", "F");
				//LoggerState ( "L", "Move", "F");
			}else {
				StateMove = false;
			}

			// Bandingkan posisi, jika kiri lebih belakang ketimbang kanan, maju
			if (distanceL < distanceR && Mathf.Abs (distanceL - distanceR) > SensivityZ && rightHand.GrabAngle > GrabToleranceWalk && leftHand.GrabAngle > GrabToleranceWalk) {
				if (DynamicMovementSpeed) {
					person.transform.position += PersonHead.transform.forward *0.1f* ( Mathf.Abs(distanceL - distanceR)) * MoveSpeed;
				} else {
					person.transform.position += PersonHead.transform.forward * 0.01f * MoveSpeed;
				}
				StateMove = true;
				//LoggerState ("R", "Move", "B");
				//LoggerState ("L", "Move", "B");
			} else {
				StateMove = false;
			}
		}

		void Scanning(Hand hand){
			 Vector3 RayOrigin = new Vector3 (hand.PalmPosition.x, hand.PalmPosition.y, hand.PalmPosition.z + zIndex);
			 Vector3 RayDirection = new  Vector3(hand.PalmNormal.x, hand.PalmNormal.y + 0.5f, hand.PalmNormal.z );

			Debug.DrawRay (RayOrigin, RayDirection);
			RaycastHit hit;
			if (hand.IsRight) {
				if (Physics.Raycast (RayOrigin, RayDirection, out hit, 10.0f) && TimeGazeR < 3.0f) {
					if (hit.collider.gameObject.tag == "Interactable") {
						TimeGazeR += Time.deltaTime;
						//LoggerState ("R", "SelectHit", "R");
					} else {
					}
				} else {
					TimeGazeR = 0.0f;
				}
			}
			if (hand.IsLeft) {
				if (Physics.Raycast (RayOrigin, RayDirection, out hit, 7.0f) && TimeGazeL < 3.0f) {
					if (hit.collider.gameObject.tag == "Interactable") {
						TimeGazeL += Time.deltaTime;
						//LoggerState ("L","SelectHit", "L");
					} else {
						TimeGazeL = 0.0f;
					}
				} else {
					TimeGazeL = 0.0f;
				}
			}

			if (Physics.Raycast (RayOrigin, RayDirection, out hit, 7.0f)) {
				if (hit.collider.gameObject.tag == "Interactable") {
					GameObject obj = hit.collider.gameObject;
					obj.GetComponent<LeapObjectProperties> ().Focus = true;
				} else {
					AllHighlight = GameObject.FindGameObjectsWithTag ("Interactable");
					foreach (GameObject highlighted in AllHighlight) {
					//	highlighted.GetComponent<LeapObjectProperties> ().Focus = false;
					}
				}
			}
				
		}

		void ScanningInspect(Hand hand){
			float handLeftNormal;
			if (hand.IsLeft) {
				PalmLeft = new Vector3 (hand.PalmPosition.x, hand.PalmPosition.y, hand.PalmPosition.z);
			}
			if (hand.IsRight) {
				PalmRight = new Vector3 (hand.PalmPosition.x, hand.PalmPosition.y, hand.PalmPosition.z);
				PalmRightDirection = new Vector3(hand.PalmNormal.x, hand.PalmNormal.y, hand.PalmNormal.z );
			}
			AllHighlight = GameObject.FindGameObjectsWithTag ("Interactable");
			foreach (GameObject highlighted in AllHighlight) {
				if (hand.IsLeft) {
					handLeftNormal = hand.PalmNormal.y;
					if (handLeftNormal > 0.4f  && highlighted.GetComponent<LeapObjectProperties> ().BoxDescription.activeInHierarchy) {
						float AngleBetweenPalm = Vector3.Angle (PalmLeft, PalmRight);
						highlighted.GetComponent<LeapObjectProperties> ().ObjectMain.transform.parent = HandleLeft.transform;
						highlighted.GetComponent<LeapObjectProperties> ().ObjectMain.SetActive (true);
						highlighted.GetComponent<LeapObjectProperties> ().ObjectMain.transform.position = PalmLeft + new Vector3(0,0.3f,0);
						//LoggerState ("L","Inspect");
					} else {
//						highlighted.GetComponent<LeapObjectProperties> ().ObjectMain.transform.parent = highlighted.GetComponent<LeapObjectProperties> ().BoxDescription.transform;
//						highlighted.GetComponent<LeapObjectProperties> ().ObjectMain.SetActive (false);
					}
				}
			}
		}

	/*	void ReturnObjectToParent(){
			AllHighlight = GameObject.FindGameObjectsWithTag ("Interactable");
			foreach (GameObject highlighted in AllHighlight) {
				highlighted.GetComponent<LeapObjectProperties> ().ObjectMain.transform.parent = highlighted.GetComponent<LeapObjectProperties> ().BoxDescription.transform;
				highlighted.GetComponent<LeapObjectProperties> ().ObjectMain.SetActive (false);
			}
		}

		void ThumbToClose(Hand hand){
			if (hand.Fingers [0].IsExtended && hand.GrabAngle > 3.0f && hand.PinchDistance > 50.0f) {
				AllHighlight = GameObject.FindGameObjectsWithTag ("Interactable");
				foreach (GameObject highlighted in AllHighlight) {
					highlighted.GetComponent<LeapObjectProperties> ().BoxDescription.SetActive (false);
				}
				AllHighlight = GameObject.FindGameObjectsWithTag ("Grabable");
				foreach (GameObject highlighted in AllHighlight) {
					highlighted.GetComponent<ObjectProperties> ().BoxDescription.SetActive (false);
				}
				AllHighlight = GameObject.FindGameObjectsWithTag ("Tutorial");
				foreach (GameObject highlighted in AllHighlight) {
					highlighted.SetActive (false);
				}
				StateScanning = false;
				TimeGazeR = 0.0f;
				TimeGazeL = 0.0f;
				if (hand.IsRight) {
					//LoggerState ("R", "Thumb");
				}
				if (hand.IsLeft) {
					//LoggerState ("L", "Thumb");
				}
			}
		} */
			

	/*	void PersonDirectionControl(Hand hand){ // Stand Mode False
			bool TriggeredR = GameObject.Find("HandAreaR").GetComponent<LeapHandArea>().IsTrigger;
			bool TriggeredL = GameObject.Find("HandAreaL").GetComponent<LeapHandArea>().IsTrigger;
			bool TriggeredT = GameObject.Find("HandAreaT").GetComponent<LeapHandArea>().IsTrigger;
			bool TriggeredD = GameObject.Find("HandAreaD").GetComponent<LeapHandArea>().IsTrigger;

			bool Up = GameObject.Find("HandAreaT").GetComponent<LeapHandArea>().ToUp;
			bool Down = GameObject.Find("HandAreaD").GetComponent<LeapHandArea>().ToDown;
			bool Left = GameObject.Find("HandAreaL").GetComponent<LeapHandArea>().ToLeft;
			bool Right = GameObject.Find("HandAreaR").GetComponent<LeapHandArea>().ToRight;

			if (hand.Confidence > 0.95f) {
				if (Right && TriggeredR) {
					person.transform.RotateAround (person.transform.position, Vector3.up, SensivityX);
					//rotationalValue += SensivityX;
				}
				if (Left && TriggeredL) {
					person.transform.RotateAround (person.transform.position, Vector3.up, -SensivityX);
					//rotationalValue -= SensivityX;
				}
				float RotationX = PersonEye.transform.localEulerAngles.x;
				if (Up && TriggeredT && (RotationX > 315 || RotationX < 46)) {
					PersonEye.transform.RotateAround (person.transform.right, -0.01f * SensivityY);
				}
				if (Down && TriggeredD && (RotationX < 45 || RotationX > 314)) {
					PersonEye.transform.RotateAround (person.transform.right, 0.01f * SensivityY);
				}
			}
		} */

		void CameraControlGyro(){
			if(SystemInfo.supportsGyroscope){
				gyro = Input.gyro;
				gyro.enabled = true;
				var att = Input.gyro.attitude * initialRotation;
				att = new Quaternion(att.x, att.y, -att.z, -att.w);
				PersonHead.transform.rotation = Quaternion.Euler (90, rotationalValue, 0) * att;
			}
		}

		void GrabObject(Hand hand){
			GameObject grabThis;
		//	GameObject grabThisParent;

			Vector3 palm = new Vector3 (hand.PalmPosition.x, hand.PalmPosition.y, hand.PalmPosition.z);
			//AllHighlight = GameObject.FindGameObjectsWithTag ("Grabable");
			foreach (GameObject highlighted in AllHighlight) {
				grabThis = highlighted;
				switch (hand.IsLeft) {
					case true:
						GrabAngleLeft = hand.GrabAngle;
						break;
					case false:
						GrabAngleRight = hand.GrabAngle;
						break;
				}
				if(hand.GrabAngle > 2.5f && hand.Confidence > 0.9f){
					isGrab = true;
				}else{
					isGrab = false;
				}
			/*	if (highlighted.GetComponent<LeapTranslate> ().Highlight == true && isGrab) {
					if (grabThis.GetComponent<LeapTranslate>().GrabBy == "Left") {
						grabThis.transform.parent = HandleLeft.transform;
						//LoggerState ("L", "Grab", "L");
					} 
					if (grabThis.GetComponent<LeapTranslate>().GrabBy == "Right") {
						grabThis.transform.parent = HandleRight.transform;
						//LoggerState ("L", "Grab", "R");
					}
					GrabPinch = true;
				}else{
					if (grabThis.GetComponent<LeapTranslate> ().GrabBy == "Left" && GrabAngleLeft < 2.5f) {
						grabThis.transform.parent = highlighted.GetComponent<LeapTranslate> ().OriginalLocation.transform;
					}
					if (grabThis.GetComponent<LeapTranslate> ().GrabBy == "Right" && GrabAngleRight < 2.5f) {
						grabThis.transform.parent = highlighted.GetComponent<LeapTranslate> ().OriginalLocation.transform;
					}
					GrabPinch = false;
				} */
			} 
		} 

		void PinchObject(Hand hand){
			GameObject pinchThis;
		//	GameObject pinchThisParent;

			Vector3 palm = new Vector3 (hand.PalmPosition.x, hand.PalmPosition.y, hand.PalmPosition.z);
			AllHighlight = GameObject.FindGameObjectsWithTag ("Pinchable");
			foreach (GameObject highlighted in AllHighlight) {
				pinchThis = highlighted;
				// Debug.Log ("Angle :" + hand.GrabAngle +" Strength :" + hand.GrabStrength)
				switch (hand.IsLeft) {
				case true:
					PinchDistanceLeft = hand.PinchDistance;
					break;
				case false:
					PinchDistanceRight = hand.PinchDistance;
					break;
				}
				if (highlighted.GetComponent<LeapTranslatePinch> ().Highlight == true && hand.PinchDistance > 2.5f && hand.Confidence > 0.9f) {
					//Logger ("Grab", hand);
					//highlighted.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeAll;
					if (pinchThis.GetComponent<LeapTranslatePinch>().GrabBy == "Left") {
						pinchThis.transform.parent = HandleLeft.transform;
						if (GrabAngleLeft < 2.5f){

							GrabPinch = true;
						}
					} 
					if (pinchThis.GetComponent<LeapTranslatePinch>().GrabBy == "Right") {
						pinchThis.transform.parent = HandleRight.transform;
						if (GrabAngleRight < 2.5f){

							GrabPinch = true;
						}
					}
					//} else if (highlighted.GetComponent<LeapTranslate> ().Grabbed == false || hand.GrabAngle < 2.5f ||(highlighted.GetComponent<LeapTranslate> ().Grabbed == true && hand.GrabAngle < 2.5f ) ) {
				}else{
					GrabPinch = false;	
					if (pinchThis.GetComponent<LeapTranslatePinch> ().GrabBy == "Left" && GrabAngleLeft < 2.5f) {
						pinchThis.transform.parent = highlighted.GetComponent<LeapTranslate> ().OriginalLocation.transform;
						//grabThis.AddComponent<Rigidbody> ();
					}
					if (pinchThis.GetComponent<LeapTranslatePinch> ().GrabBy == "Right" && GrabAngleRight < 2.5f) {
						pinchThis.transform.parent = highlighted.GetComponent<LeapTranslate> ().OriginalLocation.transform;
						//grabThis.AddComponent<Rigidbody> ();
					}
				}
			}

		}
        
		void NormalDirection(Hand hand){
			GameObject HandleR = GameObject.Find ("HandleR");
			GameObject HandleL = GameObject.Find ("HandleL");
			if (hand.IsLeft) {
				Vector3 PersonPosition = person.transform.position;
				Vector3 Palm = new Vector3 (hand.PalmNormal.x, hand.PalmNormal.y, hand.PalmNormal.z);
				//float PalmAngle = Vector3.Angle (PersonPosition, Palm);
				//Debug.Log (PalmAngle);
				if (Palm.y > 0.25f) {
					Gear.SetActive (true);
				} else {
					Gear.SetActive (false);
				}
			}
		}

		void EnableKeyboardMouse(){
			float pitch = 0.0f;
			yaw += 2.0f * Input.GetAxis("Mouse X");
			pitch -= 0.0f * Input.GetAxis("Mouse Y");

			//rotationalValue = yaw - 75;
			person.transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
		}
        
	/*	void EnableRaycast(){
			Vector3 fwd = PersonHead.transform.TransformDirection(Vector3.forward);
			//Physics.Raycast (person.transform.position, fwd, rangeRaycast);
			Debug.DrawRay (PersonHead.transform.position, fwd);
			RaycastHit hit;
			if (Physics.Raycast (PersonHead.transform.position, fwd, out hit, 7.0f)) {
				Debug.Log (hit.collider.gameObject.name);
				if (hit.collider.gameObject.tag == "Interactable") {
					GameObject obj = hit.collider.gameObject;
					obj.GetComponent<LeapObjectProperties> ().Focus = true;
				} else {
					AllHighlight = GameObject.FindGameObjectsWithTag ("Interactable");
					foreach (GameObject highlighted in AllHighlight) {
						highlighted.GetComponent<LeapObjectProperties> ().Focus = false;
					}
				}
				if (hit.collider.gameObject.tag == "Grabable") {
					GameObject obj = hit.collider.gameObject;
					obj.GetComponent<ObjectProperties> ().Focus = true;
				} else {
					AllHighlight = GameObject.FindGameObjectsWithTag ("Grabable");
					foreach (GameObject highlighted in AllHighlight) {
						highlighted.GetComponent<ObjectProperties> ().BoxDescription.SetActive (false);
					}
				}
			}else {
				AllHighlight = GameObject.FindGameObjectsWithTag ("Interactable");
				foreach (GameObject highlighted in AllHighlight) {
					highlighted.GetComponent<LeapObjectProperties> ().Focus = false;
				}
				AllHighlight = GameObject.FindGameObjectsWithTag ("Grabable");
				foreach (GameObject highlighted in AllHighlight) {
					highlighted.GetComponent<ObjectProperties> ().Focus = false;
				}
			}
		} */

		void EnableCloseInfo(){
			if (Input.GetKeyDown (KeyCode.F)) {
				AllHighlight = GameObject.FindGameObjectsWithTag ("Interactable");
				foreach (GameObject highlighted in AllHighlight) {
					highlighted.GetComponent<LeapObjectProperties> ().BoxDescription.SetActive (false);
				}
				AllHighlight = GameObject.FindGameObjectsWithTag ("Grabable");
				foreach (GameObject highlighted in AllHighlight) {
					highlighted.GetComponent<ObjectProperties> ().BoxDescription.SetActive (false);
				}
			}
		} 

		void LoggerHand(Frame frame){
			string path;
			while (!DataRecord) {
				path = @"C:\Unity\Data\" + DataRecordID + "_WorldSpace.txt";
				// This text is added only once to the file.
				if (!File.Exists (path)) {
					// Create a file to write to.
					using (StreamWriter sw = File.CreateText (path)) {
						sw.WriteLine (",,,Hand,,,,Controller,,,,,,Palm,,,,,,,,,,,,,,,,,,,Arm,,,Finger,,,,,,Grab,,Pinch,,Finger,,,,,,,,,,,,Person,,,,,,,,,,,,Head,,,");
						sw.WriteLine (",,,,,,,Position,,,Rotation,,,Position,,,Stbl Pos,,,Normal,,,Direction,,,Velocity,,,Wrist,,,,Elbow,,,Extended,,,,,,,,,,Direction,,,Tip Pos,,,Stable Tip,,,Tip Velocity,,,Position,,,Rotation,,,HandleL,,,HandleR,,,Quaternion,,,");
						sw.WriteLine ("Frame,Time,ID,Qty,L,R,Cfdnc,X,Y,Z,X,Y,Z,X,Y,Z,X,Y,Z,X,Y,Z,X,Y,Z,X,Y,Z,X,Y,Z,Width,X,Y,Z,0,1,2,3,4,X,Ang,Str,Dis,Str,X,Y,Z,X,Y,Z,X,Y,Z,X,Y,Z,X,Y,Z,X,Y,Z,X,Y,Z,X,Y,Z,X,Y,Z,W");
					}
					DataRecord = true;
				} else {
					DataRecordID++;
				}

			}
			foreach (Hand hand in frame.Hands) {
				int numHand = frame.Hands.Count;
				path = @"C:\Unity\Data\" + DataRecordID + "_WorldSpace.txt";
				using (StreamWriter sw = File.AppendText (path)) {
					sw.WriteLine (Time.frameCount + "," + TotalTime + "," + hand.Id + "," + numHand + "," + hand.IsLeft + "," + hand.IsRight + "," + hand.Confidence + ",-,-,-,-,-,-," + hand.PalmPosition.x + "," + hand.PalmPosition.y + "," + hand.PalmPosition.z + "," + hand.StabilizedPalmPosition.x + "," + hand.StabilizedPalmPosition.y + "," + hand.StabilizedPalmPosition.z + "," + hand.PalmNormal.x + "," + hand.PalmNormal.y + "," + hand.PalmNormal.z + "," + hand.Direction.x + "," + hand.Direction.y + "," + hand.Direction.z + "," + hand.PalmVelocity.x + "," + hand.PalmVelocity.y + "," + hand.PalmVelocity.z + "," + hand.WristPosition.x + "," + hand.WristPosition.y + "," + hand.WristPosition.z + "," + hand.PalmWidth + ",-,-,-," + hand.Fingers [0].IsExtended + "," + hand.Fingers [1].IsExtended + "," + hand.Fingers [2].IsExtended + "," + hand.Fingers [3].IsExtended + "," + hand.Fingers [4].IsExtended + ",-," + hand.GrabAngle + "," + hand.GrabStrength + "," + hand.PinchDistance + "," + hand.PinchStrength + ",-,-,-,-,-,-,-,-,-,-,-,-," + person.transform.position.x + "," + person.transform.position.y + "," + person.transform.position.z + "," + person.transform.rotation.x + "," + person.transform.rotation.y + "," + person.transform.rotation.z + "," + HandleLeft.transform.position.x + "," + HandleLeft.transform.position.y + "," + HandleLeft.transform.position.z + "," + HandleRight.transform.position.x + "," + HandleRight.transform.position.y + "," + HandleRight.transform.position.z + "," + PersonHead.transform.rotation.x + "," + PersonHead.transform.rotation.y + "," + PersonHead.transform.rotation.z + "," + PersonHead.transform.rotation.w + ",");
				}	
			}
		}



		void LoggerLeap (){
			Image DataLeap = provider.CurrentImage;
			 

			string path;
			while (!DataRecord) {
				path = @"C:\Unity\Data\" + DataRecordID + "_LEAP.txt";
				// This text is added only once to the file.
				if (!File.Exists (path)) {
					// Create a file to write to.
					using (StreamWriter sw = File.CreateText (path)) {
						sw.WriteLine (",,,LEAP,,,");
						sw.WriteLine (",,,,IMAGE,,");
						sw.WriteLine ("Frame,Time,Invalid,Rate,Data,H,W,");
					}
					DataRecord = true;
				} else {
					DataRecordID++;
				}
			}
			Debug.Log (DataLeap);
			if (DataLeap != null) {
				
				path = @"C:\Unity\Data\" + DataRecordID + "_LEAP.txt";
				using (StreamWriter sw = File.AppendText (path)) {
					sw.WriteLine (Time.frameCount + "," + TotalTime + "," + DataLeap.IsValid + "," + DataLeap.BytesPerPixel + "," + "-" + "," + DataLeap.Height + "," + DataLeap.Width + ",");
				}	
			} else {
				path = @"C:\Unity\Data\" + DataRecordID + "_LEAP.txt";
				using (StreamWriter sw = File.AppendText (path)) {
					sw.WriteLine (Time.frameCount + "," + TotalTime + "," + "False" + "," + "0" + "," + "-" + "," + "0" + "," + "0" + ",");
				}
			}
		}

		void LoggerState(string whichHand, string State, string DirState = null ){
			string path;
			while (!DataRecord) {
				path = @"C:\Unity\Data\" + DataRecordID + "_State.txt";
				// This text is added only once to the file.
				if (!File.Exists (path)) {
					// Create a file to write to.
					using (StreamWriter sw = File.CreateText (path)) {
						sw.WriteLine (",,,State,,,");
						sw.WriteLine ("Frame,Time,State,Hand,Confidence,whichHand,,");
					}
					DataRecord = true;
				} else {
					DataRecordID++;
				}
			}
			path = @"C:\Unity\Data\" + DataRecordID + "_State.txt";
			using (StreamWriter sw = File.AppendText (path)) {
				sw.WriteLine (Time.frameCount + "," + TotalTime + "," + State + "," + DirState + ","+ "-" + "," + whichHand + "," + "0" + ",");
			}
			//Debug.Log (hand.Confidence);

			if (Input.GetKeyDown (KeyCode.Equals)) {
				using (StreamWriter sw = File.AppendText (path)) {
					sw.WriteLine ("---,---,---,---,---,---,---,");
				}
			}
		}

		/*void LoggerReliability(string isConnected){
			string path;
			float FPS;
			while (!DataRecord) {
				path = @"C:\Unity\Data\" + DataRecordID + "_Reliability.txt";
				// This text is added only once to the file.
				if (!File.Exists (path)) {
					// Create a file to write to.
					using (StreamWriter sw = File.CreateText (path)) {
						sw.WriteLine (",,,State,,,");
						sw.WriteLine ("Frame,Time,Connected,Tris,Vert,FPS,RenderTime,ScreenBytes,ScreenRes,");
					}
					DataRecord = true;
				} else {
					DataRecordID++;
				}
			}

			deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
			FPS =  1.0f / deltaTime;
			path = @"C:\Unity\Data\" + DataRecordID + "_Reliability.txt";
			using (StreamWriter sw = File.AppendText (path)) {
				sw.WriteLine (Time.frameCount + "," + TotalTime + ","+ isConnected + "," + UnityEditor.UnityStats.triangles + ","+ UnityEditor.UnityStats.vertices + "," + FPS + "," + UnityEditor.UnityStats.renderTime + "," + UnityEditor.UnityStats.screenBytes + "," + UnityEditor.UnityStats.screenRes + ",");
			}
			if (Input.GetKeyDown (KeyCode.Equals)) {
				using (StreamWriter sw = File.AppendText (path)) {
					sw.WriteLine ("---,---,---,---,---,---,---,");
				}
			}
		}*/

		void LoggerReliabilityConf(Hand hand){
			string path;
		//	float FPS;
			while (!DataRecord) {
				path = @"C:\Unity\Data\" + DataRecordID + "_Conf.txt";
				// This text is added only once to the file.
				if (!File.Exists (path)) {
					// Create a file to write to.
					using (StreamWriter sw = File.CreateText (path)) {

						sw.WriteLine ("Frame,Time,Confidence,isR,isL,");
					}
					DataRecord = true;
				} else {
					DataRecordID++;
				}
			}
				
			path = @"C:\Unity\Data\" + DataRecordID + "_Conf.txt";
			using (StreamWriter sw = File.AppendText (path)) {
				sw.WriteLine (Time.frameCount + "," + TotalTime + ","+ hand.Confidence +","+ hand.IsRight +","+ hand.IsLeft +",");
			}
			if (Input.GetKeyDown (KeyCode.Equals)) {
				using (StreamWriter sw = File.AppendText (path)) {
					sw.WriteLine ("---,---,---,---,---,---,---,");
				}
			}
		}
	}
}